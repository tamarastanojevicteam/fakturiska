﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using S22.Imap;
using System.Net.Mail;
using System.IO;
using System.Text;

namespace Fakturiska.Controllers
{
    [Authorize (Roles ="Admin")]
    public class UsersController : Controller
    {
     
        // GET: Users
        public ActionResult Users()
        {
            Business.BusinessAccess BusinesAccess = new Business.BusinessAccess();
            
            List<Business.Views.UsersBusinessView> list = BusinesAccess.GetAllUsers();
            List<Business.Views.UsersBusinessView> model = new List<Business.Views.UsersBusinessView>();
            foreach (Business.Views.UsersBusinessView user in list)
            {
                if (user.DeleteDate == null)
                {
                    model.Add(user);
                }
            }


            return View(model);
        }

        public ActionResult CreateUser()
        {
            Business.BusinessAccess BusinesAccess = new Business.BusinessAccess();
            List<Business.Views.RolesBusinessView> roles = BusinesAccess.GetAllRoles();

            List<SelectListItem> items = new List<SelectListItem>();
            
            foreach (Business.Views.RolesBusinessView p in roles)
            {
                items.Add(new SelectListItem
                {
                    Text = p.Description,
                    Value = p.RolesID.ToString()
                });
            }
            ViewBag.Roles = items;
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(Business.Views.UsersBusinessView user)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
            user.Password = Guid.NewGuid().ToString().Substring(0, 8);
            access.CreateUser(user);

            Business.Views.UsersBusinessView idUser = access.GetUser(user.Email);

            Business.Views.RolesBusinessView role = access.GetRole(user.RoleId);
            string RoleName = role.Description;

            TempData["obj"] = idUser;
            return RedirectToAction("MyConfirm", "Account");
            
        }

        public ActionResult EditUser(string guid)
        {
            Business.Views.UsersBusinessView UserToEdit = Business.Views.UsersBusinessView.GetUserWithGUID(guid);
            Business.BusinessAccess BusinesAccess = new Business.BusinessAccess();
            List<Business.Views.RolesBusinessView> roles = BusinesAccess.GetAllRoles();

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (Business.Views.RolesBusinessView p in roles)
            {
                items.Add(new SelectListItem
                {
                    Text = p.Description,
                    Value = p.RolesID.ToString()
                });
            }
            ViewBag.Roles = items;
            Business.Views.RolesBusinessView role = BusinesAccess.GetRole(UserToEdit.RoleId);
            UserToEdit.OldRole = role.Description;
            return View(UserToEdit);
        }

        [HttpPost]
        public ActionResult EditUser(Business.Views.UsersBusinessView UserToSave)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();

            access.UpdateUser(UserToSave);
            Business.Views.RolesBusinessView role = access.GetRole(UserToSave.RoleId);

            return RedirectToAction("AddNewUser", "Account", new { UserEmail=UserToSave.Email, OldRole=UserToSave.OldRole, NewRole=role.Description });
        }

        public ActionResult DeleteUser(string guid)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
            Business.Views.UsersBusinessView NewUser = Business.Views.UsersBusinessView.GetUserWithGUID(guid);
            NewUser.DeleteDate = DateTime.Now;

            access.UpdateUser(NewUser);

            Business.Views.RolesBusinessView role = access.GetRole(NewUser.RoleId);

            return Json(Url.Action("DeleteUser", "Account",new { UserEmail=NewUser.Email, Role = role.Description}));
        }

    }
}