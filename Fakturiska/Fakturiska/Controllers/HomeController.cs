﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Web.Services;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Fakturiska.Controllers
{

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(Business.Views.UsersBusinessView user)
        {
            if (ModelState.IsValid)
            {
                Business.BusinessAccess access = new Business.BusinessAccess();
                Business.Views.UsersBusinessView NewLogInUser = access.CheckLogIn(user);

                if (NewLogInUser != null)
                {
                    Session["UsersID"] = NewLogInUser.UsersID.ToString();
                    Session["Email"] = NewLogInUser.Email.ToString();
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("LogIn");
                }

            }

            return View(user);
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Index");
        }

   
        public ActionResult RegisterUser()
        {
            return View();
        }


        public ActionResult Priority()
        {
            ViewBag.Message = "Priorities.";

            Business.BusinessAccess BusinesAccess = new Business.BusinessAccess();
            List<Business.Views.PrioritiesBusinessView> model = BusinesAccess.GetAllPriorities();

            return View(model);
        }

        public ActionResult Role()
        {
            ViewBag.Message = "Roles.";

            Business.BusinessAccess BusinesAccess = new Business.BusinessAccess();
            List<Business.Views.RolesBusinessView> model = BusinesAccess.GetAllRoles();

            return View(model);
        }

     }
}