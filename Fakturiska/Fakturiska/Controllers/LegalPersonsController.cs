﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fakturiska.Controllers
{
    [Authorize]
    public class LegalPersonsController : Controller
    {

        // GET: LegalPersons
        public ActionResult LegalPersons()
        {
            Business.BusinessAccess BusinessAccess = new Business.BusinessAccess();
            List<Business.Views.LegalPersonsBusinessView> model1 = BusinessAccess.GetAllLegalPersons();
            List<Business.Views.LegalPersonsBusinessView> model = new List<Business.Views.LegalPersonsBusinessView>();

            foreach (Business.Views.LegalPersonsBusinessView LegalPerson in model1)
            {
                if (LegalPerson.DeleteDate == null)
                {
                    model.Add(LegalPerson);
                }
            }
            
            return View(model);
        }

        [Route("/CreateNewLegalPerson")]
        public ActionResult CreateNewLegalPerson()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateNewLegalPerson(Business.Views.LegalPersonsBusinessView LegalPerson)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();

            if (string.IsNullOrWhiteSpace(LegalPerson.Name) || string.IsNullOrWhiteSpace(LegalPerson.MIB)
                || string.IsNullOrWhiteSpace(LegalPerson.PersonalNumber) || string.IsNullOrWhiteSpace(LegalPerson.PhoneNumber)
                || string.IsNullOrWhiteSpace(LegalPerson.PIB) || string.IsNullOrWhiteSpace(LegalPerson.Website)
                || string.IsNullOrWhiteSpace(LegalPerson.AccountNumber) || string.IsNullOrWhiteSpace(LegalPerson.FaxNumber)
                || string.IsNullOrWhiteSpace(LegalPerson.BankCode) || string.IsNullOrWhiteSpace(LegalPerson.Address)
                || string.IsNullOrWhiteSpace(LegalPerson.Email))
            {
                throw new Exception("All fields are required!");
            }
            else
            {
                access.CreateLegalPerson(LegalPerson);

                return RedirectToAction("LegalPersons");
            }
            
        }

        public ActionResult EditLegalPerson(string guid)
        {
            Business.Views.LegalPersonsBusinessView LegalPersonToEdit = Business.Views.LegalPersonsBusinessView.GetLegalPersonWithGuid(guid);

            return View(LegalPersonToEdit);
        }

        [HttpPost]
        public ActionResult EditLegalPerson(Business.Views.LegalPersonsBusinessView LegalPersonToSave)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();

            if (string.IsNullOrWhiteSpace(LegalPersonToSave.Name) || string.IsNullOrWhiteSpace(LegalPersonToSave.MIB)
                || string.IsNullOrWhiteSpace(LegalPersonToSave.PersonalNumber) || string.IsNullOrWhiteSpace(LegalPersonToSave.PhoneNumber)
                || string.IsNullOrWhiteSpace(LegalPersonToSave.PIB) || string.IsNullOrWhiteSpace(LegalPersonToSave.Website)
                || string.IsNullOrWhiteSpace(LegalPersonToSave.AccountNumber) || string.IsNullOrWhiteSpace(LegalPersonToSave.FaxNumber)
                || string.IsNullOrWhiteSpace(LegalPersonToSave.BankCode) || string.IsNullOrWhiteSpace(LegalPersonToSave.Address)
                || string.IsNullOrWhiteSpace(LegalPersonToSave.Email))
            {
                throw new Exception("All fields are required!");
            }
            else if(!access.checkLegalPerson(LegalPersonToSave))
            {
                throw new Exception("Changing Name, PIB or MIB of existing Legal person is not allowed!");
            }
            else
            {
                access.UpdateLegalPerson(LegalPersonToSave);

                return RedirectToAction("LegalPersons");
            }
        }

        public ActionResult GetJSONLegalPerson(string term)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
            List<string> list = access.GetAllNames();

            var LegalPersons = list.Where(Item => Item.ToLower().Contains(term.ToLower()));

            return Json(LegalPersons, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLegalPersonDetails()
        {
            try
            {
                Business.BusinessAccess access = new Business.BusinessAccess();
                List<Business.Views.LegalPersonsBusinessView> list = access.GetAllLegalPersons1();

                var json = Json(list, "application/json", JsonRequestBehavior.AllowGet);
                return json;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult GetJSONLegalPersonPIB(string term)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
            List<string> list = access.GetAllPIBs();

            var LegalPersons = list.Where(Item => Item.Contains(term));

            return Json(LegalPersons, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJSONLegalPersonMIB(string term)
        {
            Business.BusinessAccess access = new Business.BusinessAccess();
            List<string> list = access.GetAllMIBs();

            var LegalPersons = list.Where(Item => Item.Contains(term));

            return Json(LegalPersons, JsonRequestBehavior.AllowGet);
        }
        
    }
}