﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using S22.Imap;
using System.Net.Mail;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

namespace Fakturiska.Models
{
    public class JobClass:IJob
    {
        static DateTime date;
        public void Execute(IJobExecutionContext context)
        {
            using (ImapClient client = new ImapClient("imap.gmail.com", 993, "fakturiska@gmail.com", "fakturiska1!", AuthMethod.Login, true))
            {  
                Business.BusinessAccess access = new Business.BusinessAccess();
                
                date = JobScheduler.lastDate;
                IEnumerable<uint> uids = client.Search(SearchCondition.SentSince(date));
                
                string newLastDate = "";
                newLastDate = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");


                string s = System.Web.HttpRuntime.BinDirectory;
                string[] path1 = s.Split('\\');
                string ss = "";
                string dest = ""; 

                for (int i = 0; i < path1.Count() - 2; i++)
                {
                    ss += path1[i] + "\\";
                    dest += path1[i] + "\\";
                }
                ss += "Files\\datetime.bin";
                dest += "Data\\";

                using (BinaryWriter writer = new BinaryWriter(File.Open(ss, FileMode.OpenOrCreate)))
                {
                    writer.Write(newLastDate);
                }

                JobScheduler.lastDate = DateTime.Parse(newLastDate);
           
                 IEnumerable<MailMessage> messages = client.GetMessages(uids, (Bodypart part) =>
                {
                    if (part.Disposition.Type == ContentDispositionType.Attachment)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                });
                
                foreach (MailMessage mail in messages)
                {
                    if (mail.Date().Value >= date)
                    {
                        foreach (Attachment attachment in mail.Attachments)
                        {
                            string[] name1 = attachment.Name.Split('.');
                            if (name1.Last().Equals("pdf") || name1.Last().Equals("jpg") || name1.Last().Equals("jpeg") || name1.Last().Equals("png"))
                            {

                                string guid = Guid.NewGuid().ToString();
                                
                                string destinationFile = dest + guid + attachment.Name;
                       

                                if (!File.Exists(destinationFile))
                                {
                                    byte[] allBytes = new byte[attachment.ContentStream.Length];
                                    int bytesRead = attachment.ContentStream.Read(allBytes, 0, (int)attachment.ContentStream.Length);
                                    BinaryWriter writer = new BinaryWriter(new FileStream(destinationFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None));
                                    writer.Write(allBytes);
                                    writer.Close();

                                    Business.Views.InvoiceBusinessView invoice = new Business.Views.InvoiceBusinessView();
                                    invoice.FilePath = "~/Data/"+guid+attachment.Name;

                                    
                                    int userId = access.GetUserId(mail.From.Address);
                                    if (userId > 0)
                                    {
                                        invoice.UserID = userId;
                                    }
                                    else
                                    {
                                        Business.Views.UsersBusinessView user = access.GetFirstAdmin();
                                        invoice.UserID = user.UsersID;
                                    }
                                    

                                    invoice.GUID = Guid.Parse(guid);
                                    access.CreateInvoice(invoice);
                                }
                            }
                        }

                        
                    }


                }
                

            }
        }

    }
}