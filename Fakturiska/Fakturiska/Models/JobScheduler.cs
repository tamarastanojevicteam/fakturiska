﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace Fakturiska.Models
{
    public class JobScheduler
    {
        public static DateTime lastDate;
        public static void Start()
        {
            string s = System.Web.HttpRuntime.BinDirectory;
            string[] path1 = s.Split('\\');
            string ss = "";
            for (int i = 0; i < path1.Count() - 2; i++)
            {
                ss += path1[i] + "\\";
            }
            ss += "Files\\datetime.bin";
            string lastDateString = null;
            using (BinaryReader reader = new BinaryReader(File.Open(ss, FileMode.Open)))
            {
                lastDateString = reader.ReadString();
            }
            
            lastDate = DateTime.Parse(lastDateString);

            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<JobClass>().Build();

            ITrigger trigger = TriggerBuilder.Create()
            .WithIdentity("trigger1", "group1")
            .StartNow()
            .WithSimpleSchedule(x => x
            .WithIntervalInSeconds(60)
            .RepeatForever())
            .Build();


            scheduler.ScheduleJob(job, trigger);
        }
    }
}