﻿using Fakturiska.Business.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fakturiska.Database;

namespace Fakturiska.Business
{
    public class BusinessAccess
    {
        public List<PrioritiesBusinessView> GetAllPriorities()
        {
            return PrioritiesBusinessView.GetAllPriorities();
        }

        public PrioritiesBusinessView GetPriority(int id)
        {
            return PrioritiesBusinessView.GetPriority(id);
        }

        public List<RolesBusinessView> GetAllRoles()
        {
            return RolesBusinessView.GetAllRoles();
        }

        public RolesBusinessView GetRole(int id)
        {
            return RolesBusinessView.GetRole(id);
        }

        public UsersBusinessView CheckLogIn(UsersBusinessView user)
        {
            return UsersBusinessView.CheckLogInUser(user);
        }

        public List<UsersBusinessView> GetAllUsers()
        {
            return UsersBusinessView.GetAllUsers();
        }

        public UsersBusinessView GetUser(int id)
        {
            return UsersBusinessView.GetUser(id);
        }
        public UsersBusinessView GetUser(string email)
        {
            return UsersBusinessView.GetUser(email);
        }

        public void AchiveInvoice(string guid)
        {
            InvoiceBusinessView.AchiveInvoice(guid);
        }

        public void DeleteInvoice(string guid)
        {
            InvoiceBusinessView.DeleteInvoice(guid);
        }

        public List<InvoiceBusinessView> GetAllInvoices()
        {
            return InvoiceBusinessView.GetAllInvoices();
        }

        public InvoiceBusinessView GetInvoice(int id)
        {
            return InvoiceBusinessView.GetInvoice(id);
        }

        public List<LegalPersonsBusinessView> GetAllLegalPersons()
        {
            return LegalPersonsBusinessView.GetAllLegalPersons();
        }

        public LegalPersonsBusinessView GetLegalPerson(int id)
        {
            return LegalPersonsBusinessView.GetLegalPerson(id);
        }

        public void CreateLegalPerson(Views.LegalPersonsBusinessView newLegalPerson)
        {
            Views.LegalPersonsBusinessView.CreateDB(newLegalPerson);
        }
        
        public bool ExistUser(string address)
        {
            return Views.UsersBusinessView.Exist(address);
        }

        public void CreateInvoice(InvoiceBusinessView invoice)
        {
            bool legalPerson1NoExists = false;
            bool legalPerson2NoExists = false;

            if (invoice.LegalPerson != null)
            {
                if (string.IsNullOrWhiteSpace(invoice.LegalPerson.GUID.ToString()))
                {
                    invoice.LegalPerson = null;
                    invoice.RecieverId = null;
                }
                else if (invoice.LegalPerson.LegalPersonsID != 0 || invoice.LegalPerson.Name != null || invoice.LegalPerson.PIB != null)
                {
                    Guid guid = invoice.LegalPerson.GUID;
                    if (!guid.Equals(Guid.Empty))
                    {
                        if (LegalPersonsBusinessView.CheckLegalPerson((invoice.LegalPerson)))
                        {
                            LegalPersonsBusinessView legal = LegalPersonsBusinessView.GetLegalPersonWithGuid(invoice.LegalPerson.GUID.ToString());
                            Database.LegalPerson newLegal = LegalPersonsBusinessView.ReturnToLegalPerson(legal);
                            invoice.LegalPerson = newLegal;
                            invoice.RecieverId = newLegal.LegalPersonsID;
                        }
                        else
                        {
                            throw new Exception("Changing Name, PIB or MIB of existing Receiver is not allowed!");
                        }
                    }
                    else
                    {
                        legalPerson1NoExists = true;
                    }
                }
                else
                {
                    invoice.RecieverId = null;
                }
            }
            else
            {
                invoice.RecieverId = null;
            }


            if (invoice.LegalPerson1 != null)
            {
                if (string.IsNullOrWhiteSpace(invoice.LegalPerson1.GUID.ToString()))
                {
                    invoice.LegalPerson1 = null;
                    invoice.PayerId = null;
                }
                else if (invoice.LegalPerson1.LegalPersonsID != 0 || invoice.LegalPerson1.Name != null || invoice.LegalPerson1.PIB != null)
                {
                    if (!invoice.LegalPerson1.GUID.Equals(Guid.Empty))
                    {
                        if (LegalPersonsBusinessView.CheckLegalPerson(invoice.LegalPerson1))
                        {
                            LegalPersonsBusinessView legal = LegalPersonsBusinessView.GetLegalPersonWithGuid(invoice.LegalPerson1.GUID.ToString());
                            Database.LegalPerson newLegal = LegalPersonsBusinessView.ReturnToLegalPerson(legal);
                            invoice.LegalPerson1 = newLegal;
                            invoice.PayerId = newLegal.LegalPersonsID;
                        }
                        else
                        {
                            throw new Exception("Changing Name, PIB or MIB of existing Payer is not allowed!");
                        }
                    }
                    else
                    {
                        legalPerson2NoExists = true;
                    }
                }
                else
                {
                    invoice.PayerId = null;
                }

            }
            else
            {
                invoice.PayerId = null;
            }

            if (invoice.User == null && invoice.UserID != 0)
            {
                invoice.User = Database.DataAccess.DAUsers.GetUser(invoice.UserID);
            }


            invoice.Date = DateTime.Now;

            InvoiceBusinessView.CreateInvoice(invoice, legalPerson1NoExists, legalPerson2NoExists);
        }

        public bool checkLegalPerson(LegalPersonsBusinessView legalPersonToSave)
        {
            return LegalPersonsBusinessView.checkLegalPerson(legalPersonToSave);
        }

        public int GetUserId(string address)
        {
            return UsersBusinessView.GetUserId(address);
        }

        public UsersBusinessView GetFirstAdmin()
        {
            return UsersBusinessView.GetFirstAdmin();
        }

        public void UpdateUser(UsersBusinessView userToSave)
        {
            UsersBusinessView.UpdateUser(userToSave);
        }

        public void CreateUser(UsersBusinessView user)
        {
            UsersBusinessView.CreateUser(user);
        }

        public void UpdateLegalPerson(LegalPersonsBusinessView legalPersonToSave)
        {
            LegalPersonsBusinessView.UpdateLegalPerson(legalPersonToSave);
        }

        public void UpdateInvoice(InvoiceBusinessView allToSave)
        {
            
            if (allToSave.LegalPerson != null)
            {
                if (allToSave.LegalPerson.GUID.Equals(""))
                {
                    allToSave.LegalPerson = null;
                    allToSave.RecieverId = null;
                }
                
            }
            else
            {
                allToSave.RecieverId = null;
            }

            if (allToSave.LegalPerson1 != null)
            {
                if (allToSave.LegalPerson1.GUID.Equals(""))
                {
                    allToSave.LegalPerson1 = null;
                    allToSave.PayerId= null;
                }
                
            }
            else
            {
                allToSave.PayerId = null;
            }

            UsersBusinessView user = UsersBusinessView.GetUser(allToSave.User.Email);
            allToSave.UserID = user.UsersID;

            InvoiceBusinessView.UpdateInvoice(allToSave);

        }

        public object GetJSONLegalPerson(string prefix)
        {
            return LegalPersonsBusinessView.GetJSONLegalPerson(prefix);
        }

        public List<string> GetAllNames()
        {
            return LegalPersonsBusinessView.GetAllNames();
        }

        public List<LegalPersonsBusinessView> GetAllLegalPersons1()
        {
            return LegalPersonsBusinessView.GetAllLegalPersons1();
        }

        public List<string> GetAllPIBs()
        {
            return LegalPersonsBusinessView.GetAllPIBs();
        }

        public List<string> GetAllMIBs()
        {
            return LegalPersonsBusinessView.GetAllMIBs();
        }

        public UsersBusinessView GetUserWithGUID(string uid)
        {
            return UsersBusinessView.GetUserWithGUID(uid);
        }

        public LegalPersonsBusinessView GetLegalPersonWithGuid(string guid)
        {
            return LegalPersonsBusinessView.GetLegalPersonWithGuid(guid);
        }
        public InvoiceBusinessView GetInvoiceWithGuid(string guid)
        {
            return InvoiceBusinessView.GetInvoiceWithGuid(guid);
        }

        public void ChangePassword(string name, string newPassword)
        {
            UsersBusinessView.ChangePassword(name, newPassword);
        }
    }

}
