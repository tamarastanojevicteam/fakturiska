﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Business.Views
{
    public class PrioritiesBusinessView
    {
        public int PrioritiesID { get; set; }

        [Display(Name = "Priority")]
        public string Description { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual List<InvoiceBusinessView> Invoices { get; set; }

        public PrioritiesBusinessView(int id, string desc, System.DateTime d)
        {
            this.PrioritiesID = id;
            this.Description = desc;
            this.DeleteDate = d;
        }

        public PrioritiesBusinessView(Database.Priority p)
        {
            this.PrioritiesID = p.PrioritiesID;
            this.DeleteDate = p.DeleteDate;
            this.Description = p.Description;
            Invoices = new List<InvoiceBusinessView>();

            List<InvoiceBusinessView> invoices = InvoiceBusinessView.GetAllInvoices();
            foreach (InvoiceBusinessView invoice in invoices)
            {
                if (invoice.PriorityId == this.PrioritiesID)
                {
                    Invoices.Add(invoice);
                }
            }

        }

        public static List<PrioritiesBusinessView> GetAllPriorities()
        {
            List<PrioritiesBusinessView> BusinessPriorities = new List<PrioritiesBusinessView>();
            List<Database.Priority> DatabasePriorities = Database.DataAccess.DAPriorities.GetAllPriorities();

            foreach (Database.Priority priority in DatabasePriorities)
            {
                PrioritiesBusinessView NewPriority = new PrioritiesBusinessView(priority);
                BusinessPriorities.Add(NewPriority);
            }

            return BusinessPriorities;
        }

        public static PrioritiesBusinessView GetPriority(int id)
        {
            PrioritiesBusinessView Priority = new PrioritiesBusinessView(Database.DataAccess.DAPriorities.GetPriority(id));

            return Priority;
        }

    }

}
