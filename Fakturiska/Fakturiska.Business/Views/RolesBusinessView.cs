﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Business.Views
{
    public class RolesBusinessView
    {
        public int RolesID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsersBusinessView> Users { get; set; }


        public RolesBusinessView(int id, string des, System.DateTime date)
        {
            this.RolesID = id;
            this.Description = des;
            this.DeleteDate = date;
        }

        public RolesBusinessView(Database.Role role)
        {
            this.RolesID = role.RolesID;
            this.Description = role.Description;
            this.DeleteDate = role.DeleteDate;

            /*         List<UsersBusinessView> users = UsersBusinessView.GetAllUsers();
                     foreach(UsersBusinessView user in users)
                     {
                         if(user.RoleId==this.RolesID)
                         {
                             Users.Add(user);
                         }
                     }
                */
        }

        public static List<RolesBusinessView> GetAllRoles()
        {
            List<RolesBusinessView> BusinessRoles = new List<RolesBusinessView>();
            List<Database.Role> DatabaseRoles = Database.DataAccess.DARoles.GetAllRoles();

            foreach (Database.Role role in DatabaseRoles)
            {
                RolesBusinessView NewRole = new RolesBusinessView(role);
                BusinessRoles.Add(NewRole);
            }

            return BusinessRoles;
        }

        public static RolesBusinessView GetRole(int id)
        {
            RolesBusinessView Role = new RolesBusinessView(Database.DataAccess.DARoles.GetRole(id));

            return Role;
        }



    }

}
