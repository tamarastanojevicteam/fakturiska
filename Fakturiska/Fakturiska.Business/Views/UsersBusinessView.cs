﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Business.Views
{
    public class UsersBusinessView
    {
        public int UsersID { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }
        
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Required]
        [Display(Name = "Role ID")]
        public int RoleId { get; set; }
        public Nullable<System.DateTime> DeleteDate { get; set; }
        public System.Guid GUID { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InvoiceBusinessView> Invoices { get; set; }
        [Display(Name = "Role")]
        public string RoleName { get; set; }
        public virtual Database.Role Role { get; set; }
        public string OldPassword { get; set; }
        public string OldRole { get; set; }
        public UsersBusinessView()
        {

        }

        public UsersBusinessView(Database.User user)
        {
            this.UsersID = user.UsersID;
            this.Email = user.Email;
            this.Password = user.Password;
            this.RoleId = user.RoleId;
            this.DeleteDate = user.DeleteDate;
            this.GUID = user.GUID;
            this.Role = Database.DataAccess.DARoles.GetRole(this.RoleId);
            this.RoleName = this.Role.Description;
            this.Invoices = new List<InvoiceBusinessView>();
            List<InvoiceBusinessView> invoices = InvoiceBusinessView.GetAllInvoices();
            foreach (InvoiceBusinessView invoice in invoices)
            {
                if (invoice.UserID == this.UsersID)
                {
                    this.Invoices.Add(invoice);
                }
            }
        }

        public static Database.User ReturnToUser(UsersBusinessView user)
        {
            Database.User ReturnUser = new Database.User();
            ReturnUser.UsersID = user.UsersID;
            ReturnUser.Email = user.Email;
            ReturnUser.DeleteDate = user.DeleteDate;
            ReturnUser.Password = user.Password;
            ReturnUser.RoleId = user.RoleId;
            ReturnUser.GUID = user.GUID;

            return ReturnUser;
        }

        public static bool Exist(string address)
        {
            return Database.DataAccess.DAUsers.Exist(address);
        }

        public static UsersBusinessView CheckLogInUser(UsersBusinessView user)
        {
            UsersBusinessView ReturnUser = new UsersBusinessView();
            Database.User NewUser = Database.DataAccess.DAUsers.GetUser(user.Email, user.Password);
            if (NewUser != null)
                ReturnUser = new UsersBusinessView(NewUser);
            else
                ReturnUser = null;
            return ReturnUser;
        }

        public static List<UsersBusinessView> GetAllUsers()
        {
            List<UsersBusinessView> BusinessUsers = new List<UsersBusinessView>();
            List<Database.User> DatabaseUsers = Database.DataAccess.DAUsers.GetAllUsers();

            foreach (Database.User user in DatabaseUsers)
            {
                UsersBusinessView NewUser = new UsersBusinessView(user);
                BusinessUsers.Add(NewUser);
            }

            return BusinessUsers;
        }
        public static int GetUserId(string mail)
        {
            return Database.DataAccess.DAUsers.GetUserId(mail);
        }


        public static UsersBusinessView GetUser(int id)
        {
            UsersBusinessView ReturnUser = new UsersBusinessView(Database.DataAccess.DAUsers.GetUser(id));

            return ReturnUser;
        }

        public static UsersBusinessView GetUser(string email)
        {
            UsersBusinessView ReturnUser = new UsersBusinessView(Database.DataAccess.DAUsers.GetUser(email));

            return ReturnUser;
        }

        public static void UpdateUser(UsersBusinessView userToSave)
        {
            Database.User NewUser = UsersBusinessView.ReturnToUser(userToSave);

            Database.DataAccess.DAUsers.UpdateDB(NewUser);

        }

        public static void CreateUser(UsersBusinessView user)
        {
            Database.User NewUser = new Database.User();
            NewUser = UsersBusinessView.ReturnToUser(user);
            NewUser.GUID = Guid.NewGuid();
            NewUser.DeleteDate = null;

            Database.DataAccess.DAUsers.CreateDB(NewUser);

        }

        public static UsersBusinessView GetUserWithGUID(string uid)
        {
            Database.User NewUser = Database.DataAccess.DAUsers.GetUserWithGUID(uid);
            UsersBusinessView ReturnUser = new UsersBusinessView(NewUser);
            return ReturnUser;
        }

        public static UsersBusinessView GetFirstAdmin()
        {
            Database.User NewUser = Database.DataAccess.DAUsers.GetFirstAdmin();
            UsersBusinessView ReturnUser = new UsersBusinessView(NewUser);
            return ReturnUser;
        }

        public static void ChangePassword(string name, string newPassword)
        {
            Database.DataAccess.DAUsers.ChangePassword(name, newPassword);
        }
    }

}
