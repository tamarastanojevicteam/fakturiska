﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Database.DataAccess
{
    public class DAPriorities
    {
        public static List<Priority> GetAllPriorities()
        {
            List<Priority> list = new List<Priority>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Priorities.ToList();
            }

            return list;
        }

        public static Priority GetPriority(int? id)
        {
            List<Priority> list = new List<Priority>();
            Priority ReturnPriority = new Priority();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {

                list = entities.Priorities.ToList();

                var sql = (from priority in list
                           where priority.PrioritiesID == id
                           select priority).ToList();

                if (sql != null)
                {
                    ReturnPriority = sql.FirstOrDefault();
                    return ReturnPriority;
                }

            }

            return null;
        }

    }

}
