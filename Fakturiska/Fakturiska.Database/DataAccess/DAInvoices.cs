﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fakturiska.Database.DataAccess
{
    public class DAInvoices
    {
        public static List<Invoice> GetAllInvoices()
        {
            List<Invoice> list = new List<Invoice>();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Invoices.ToList();
            }

            return list;
        }
        public static Invoice GetInvoice(int id)
        {
            List<Invoice> list = new List<Invoice>();
            Invoice ReturnInvoice = new Invoice();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Invoices.ToList();

                var sql = (from invoice in list
                           where invoice.InvoicesID == id
                           select invoice).ToList();

                if (sql != null)
                {
                    ReturnInvoice = sql.FirstOrDefault();
                    return ReturnInvoice;
                }
            }

            return null;
        }
        public static Invoice GetInvoiceWithGuid(string guid)
        {
            List<Invoice> list = new List<Invoice>();
            Invoice ReturnInvoice = new Invoice();

            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                list = entities.Invoices.ToList();

                var sql = (from invoice in list
                           where invoice.GUID == Guid.Parse(guid)
                           select invoice).ToList();

                if (sql != null)
                {
                    ReturnInvoice = sql.FirstOrDefault();
                    return ReturnInvoice;
                }
            }

            return null;
        }

        public static void DeleteInvoice(string guid)
        {
            Guid ggg = Guid.Parse(guid);
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from invoice in entities.Invoices
                           where invoice.GUID == ggg
                           select invoice).FirstOrDefault();

                if (sql != null)
                {
                    sql.DeleteDate = DateTime.Now;
                    entities.SaveChanges();
                }
            }
        }

        public static void ArchiveInvoice(string guid)
        {
            Guid ggg = Guid.Parse(guid);
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                var sql = (from invoice in entities.Invoices
                           where invoice.GUID == ggg
                           select invoice).FirstOrDefault();

                if (sql != null)
                {
                    sql.Archive = 1;
                    entities.SaveChanges();
                }
            }
        }

        public static void UpdateInvoice(Invoice invoice, LegalPerson legalPerson1, LegalPerson legalPerson2)
        {
            try
            {
                using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
                {
                    using (var transaction = entities.Database.BeginTransaction())
                    {
                        try
                        {
                            if (legalPerson1 != null)
                            {
                                if (legalPerson1.GUID == null || legalPerson1.GUID.Equals(Guid.Empty))
                                    legalPerson1.GUID = Guid.NewGuid();

                                entities.LegalPersons.Add(legalPerson1);
                                entities.SaveChanges();

                                var sql1 = (from lp in entities.LegalPersons
                                           where lp.GUID == legalPerson1.GUID
                                           select lp).FirstOrDefault();

                                if (sql1 != null)
                                    invoice.RecieverId = sql1.LegalPersonsID;
                            }
                            if (legalPerson2 != null)
                            {
                                if (legalPerson2.GUID == null || legalPerson2.GUID.Equals(Guid.Empty))
                                    legalPerson2.GUID = Guid.NewGuid();

                                entities.LegalPersons.Add(legalPerson2);
                                entities.SaveChanges();

                                var sql1 = (from lp in entities.LegalPersons
                                           where lp.GUID == legalPerson2.GUID
                                           select lp).FirstOrDefault();

                                if (sql1 != null)
                                    invoice.PayerId = sql1.LegalPersonsID;
                            }

                            var sql = (from inv in entities.Invoices
                                       where inv.InvoicesID == invoice.InvoicesID
                                       select inv).FirstOrDefault();

                            if (sql != null)
                            {
                                sql.Date = invoice.Date;
                                sql.Sum = invoice.Sum;
                                sql.InvoiceEstimate = invoice.InvoiceEstimate;
                                sql.InvoiceTotal = invoice.InvoiceTotal;
                                sql.Incoming = invoice.Incoming;
                                sql.Paid = invoice.Paid;
                                sql.Risk = invoice.Risk;
                                sql.PriorityId = invoice.PriorityId;
                                sql.RecieverId = invoice.RecieverId;
                                sql.PayerId = invoice.PayerId;
                                sql.FilePath = invoice.FilePath;
                                sql.UserID = invoice.UserID;
                                if (invoice.Archive == null)
                                    sql.Archive = 0;
                                else
                                {
                                    sql.Archive = invoice.Archive;
                                }
                                sql.DeleteDate = invoice.DeleteDate;
                                sql.PaymentDate = invoice.PaymentDate;

                            }
                            else
                            {
                                throw new Exception("Update failed");
                            }

                            entities.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                        }
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                string s = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    
                    foreach (var ve in eve.ValidationErrors)
                    {
                        s += ("- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage);
                    }
                }
                throw new Exception(s);
            }
        }

        public static void CreateInvoice(Invoice newInvoice, LegalPerson legalPerson1, LegalPerson legalPerson2)
        {
            using (Database.FakturiskaDBEntities entities = new FakturiskaDBEntities())
            {
                using (var transaction = entities.Database.BeginTransaction())
                {
                    try
                    {
                        if(legalPerson1!=null)
                        {
                            if (legalPerson1.GUID == null || legalPerson1.GUID.Equals(Guid.Empty))
                                legalPerson1.GUID = Guid.NewGuid();
                           
                            entities.LegalPersons.Add(legalPerson1);
                            entities.SaveChanges();

                            var sql = (from lp in entities.LegalPersons
                                       where lp.GUID == legalPerson1.GUID
                                       select lp).FirstOrDefault();

                            if(sql!=null)
                                newInvoice.RecieverId = sql.LegalPersonsID;
                        }
                        if(legalPerson2!=null)
                        {
                            if (legalPerson2.GUID == null || legalPerson2.GUID.Equals(Guid.Empty))
                                legalPerson2.GUID = Guid.NewGuid();

                            entities.LegalPersons.Add(legalPerson2);
                            entities.SaveChanges();

                            var sql = (from lp in entities.LegalPersons
                                       where lp.GUID == legalPerson2.GUID
                                       select lp).FirstOrDefault();

                            if (sql != null)
                                newInvoice.PayerId = sql.LegalPersonsID;
                        }
                        
                        entities.Invoices.Add(newInvoice);

                        entities.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception){
                        transaction.Rollback();
                    }
                }
            }
        }

       
    }

}
